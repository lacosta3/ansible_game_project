#!/bin/bash

username=ec2-user
echo "Checking Results"
#Check if dir exists
for i in {1..3}
do
	if [[ `ssh node$i test -d /home/$username/lab_results && echo exists` ]] ; then
		echo "Directory exists on Node$i"
	else
		echo "Directory does not exists on Node$i"
		echo "Game Over, try again!"
		exit
	fi
done

#Check dir permission
for i in {1..3}
do
dir_per=$(ssh node$i "stat -c '%a' /home/$username/lab_results")
#echo $dir_per
if [ "$dir_per" != "755" ]; then
	echo "Directory permission are not correct."
	echo "Game Over, try again!"
	exit
else
	echo "Dir Permission are correct."
fi
done

#Check gvim application
for i in {1..3}
do
	if [[ `ssh node$i test -f /usr/bin/gvim && echo exists` ]] ; then
		echo "Gvim exists on Node$i"
	else
		exit
	fi
done

#Check file and file permission
for i in {1..3}
do
	if [[ `ssh node$i test -f /home/$username/lab_results/cpuinfo && echo exists` ]] ; then
		echo "cpuinfo file exists on Node$i"
	else
		echo "cpuinfo file does not exists on Node$i"
		echo "Game Over, try again!"
		exit
	fi
done

for i in {1..3}
do
dir_per=$(ssh node$i "stat -c '%a' /home/$username/lab_results/cpuinfo")
#echo $dir_per
if [ "$dir_per" != "644" ]; then
	echo "cpuinfo file permission are not correct."
	echo "Game Over, try again!"
	exit
else
	echo "Dir Permission are correct."
fi
done
echo "----------------------------------"
echo "Congrats! All tasks are completed!"
echo "Secret: Red Hat"
echo "Completed: $(date)"
